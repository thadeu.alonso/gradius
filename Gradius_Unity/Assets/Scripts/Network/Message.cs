﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum MessageType
{
    PLAYER_CONNECTED,
    PLAYER_MOVE,
    PLAYER_SHOOT,
    PLAYER_DIES,
    PLAYER_LOGGED,
    OBSTACLE_SPAWNED,
    OBSTACLE_DESTROYED,
    GAME_STARTED,
    GAME_OVER
}

[Serializable]
public class Message
{
    public MessageType MessageType { get; set; }
    public int PlayerId { get; set; }
}

[Serializable]
public class GameStartedMsg : Message
{
    public GameStartedMsg()
    {
        MessageType = MessageType.GAME_STARTED;
    }
}

[Serializable]
public class GameOverMsg : Message
{
    public GameOverMsg()
    {
        MessageType = MessageType.GAME_OVER;
    }
}

[Serializable]
public class PlayerLoggedMsg : Message
{
    public int CodP1 { get; set; }
    public int CodP2 { get; set; }

    public PlayerLoggedMsg(int _codP1, int _codP2)
    {
        MessageType = MessageType.PLAYER_LOGGED;
        CodP1 = _codP1;
        CodP2 = _codP2;
    }
}

[Serializable]
public class PlayerConnectedMsg : Message
{
    public PlayerConnectedMsg(int _newId)
    {
        MessageType = MessageType.PLAYER_CONNECTED;
        PlayerId = _newId;
    }
}

[Serializable]
public class PlayerMoveMsg : Message
{
    public int DirX { get; set; }
    public int DirY { get; set; }

    public PlayerMoveMsg(int _playerId, int _dirX, int _dirY)
    {
        MessageType = MessageType.PLAYER_MOVE;
        PlayerId = _playerId;
        DirX = _dirX;
        DirY = _dirY;
    }
}

[Serializable]
public class PlayerShootMsg : Message
{
    public Vector2 ShootPosition { get; set; }

    public PlayerShootMsg(int _playerId, Vector2 _shootPos)
    {
        MessageType = MessageType.PLAYER_SHOOT;
        PlayerId = _playerId;
        ShootPosition = _shootPos;
    }
}

[Serializable]
public class PlayerDiesMsg : Message
{
    public PlayerDiesMsg(int playerId)
    {
        MessageType = MessageType.PLAYER_DIES;
        PlayerId = playerId;
    }
}

[Serializable]
public class ObstacleSpawnedMsg : Message
{
    public int ObstacleIndex { get; set; }
    public int PositionIndex { get; set; }

    public ObstacleSpawnedMsg(int enemyIndex, int randPos)
    {
        MessageType = MessageType.OBSTACLE_SPAWNED;
        ObstacleIndex = enemyIndex;
        PositionIndex = randPos;
    }
}

[Serializable]
public class ObstacleDestroyedMsg : Message
{
    public int ShooterID { get; set; }
    public int Points { get; set; }

    public ObstacleDestroyedMsg(int _shooterID, int _points)
    {
        MessageType = MessageType.OBSTACLE_DESTROYED;
        ShooterID = _shooterID;
        Points = _points;
    }
}