﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float bulletSpeed = 800f;
    [SerializeField] bool enemyBullet;

    public int ShooterId { get; set; }

    private Vector2 direction;

    private void Start()
    {
        direction = enemyBullet ? Vector2.left : Vector2.right;
    }

    private void Update()
    {
        transform.Translate(direction * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!enemyBullet)
        {
            if(collision.gameObject.tag == "Enemy")
                gameObject.SetActive(false);
        }

        if(collision.gameObject.tag == "Limit")
        {
            gameObject.SetActive(false);
        }

        if(collision.gameObject.tag == "GarbageCollector")
        {
            Destroy(gameObject);
        }
    }
}