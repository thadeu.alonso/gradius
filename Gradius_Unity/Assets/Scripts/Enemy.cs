﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Obstacle
{
    [Header("Shoot Settings")]
    [SerializeField] bool isShip;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform firePoint;
    [SerializeField] float shootDelay;
    private float shootTimer;

    private void Start()
    {
        shootTimer = shootDelay;
    }

    protected override void Update()
    {
        base.Update();

        if (shootTimer <= 0f)
        {
            Shoot();
            shootTimer = shootDelay;
        }
        else
        {
            shootTimer -= Time.deltaTime;
        }
    }

    private void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
    }
}