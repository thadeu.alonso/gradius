﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Network Settings")]
    [SerializeField] int playerId = -1;
    [SerializeField] bool isLocalPlayer = false;

    [Header("Movement Settings")]
    [SerializeField] float moveSpeed = 8f;

    [Header("Shooting Settings")]
    [SerializeField] Transform firePoint;
    [SerializeField] float shootDelay = 0.5f;
    private float shootTimer = 0f;
    private ObjectPooler bulletPool;

    [Header("Gameplay Settings")]
    [SerializeField] int maxLifes = 3;

    public bool IsLocalPlayer
    {
        get
        {
            return isLocalPlayer;
        }
        set
        {
            isLocalPlayer = value;
        }
    }
    public int PlayerId { get { return playerId; } set { playerId = value; } }

    public int Lifes { get; set; }
    public int Points { get; set; }
    public int Deaths { get; set; }

    public bool IsDead { get; set; }

    public int codJogador;

    private void OnEnable()
    {
        EventManager.StartListening<OnPlayerMoveEvent>(OnPlayerMovedEvent);
        EventManager.StartListening<OnPlayerShootEvent>(OnPlayerShootsEvent);
    }

    private void OnDisable()
    {
        EventManager.StopListening<OnPlayerMoveEvent>(OnPlayerMovedEvent);
        EventManager.StopListening<OnPlayerShootEvent>(OnPlayerShootsEvent);
    }

    private void OnPlayerMovedEvent(EventMessage obj)
    {
        OnPlayerMoveEvent msg = (OnPlayerMoveEvent)obj;

        if(!isLocalPlayer)
            Move(msg.Input);
    }

    private void OnPlayerShootsEvent(EventMessage obj)
    {
        OnPlayerShootEvent msg = (OnPlayerShootEvent)obj;

        if (!isLocalPlayer)
            Shoot();
    }

    private void Awake()
    {
        bulletPool = GetComponent<ObjectPooler>();
        Lifes = maxLifes;
    }

    private void Update()
    {
        if (!IsLocalPlayer)
            return;

        CheckMoveInput();
        CheckShootInput();
    }

    private void CheckShootInput()
    {
        if(Input.GetKey(KeyCode.Space))
        {
            if(shootTimer <= 0f)
            {
                Shoot();
                shootTimer = shootDelay;
                NetworkClient.Instance.Send(new PlayerShootMsg(playerId, transform.position));
            }
            else
            {
                shootTimer -= Time.deltaTime;
            }
        }
    }

    private void CheckMoveInput()
    {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (input != Vector2.zero)
        {
            Move(input);
            NetworkClient.Instance.Send(new PlayerMoveMsg(playerId, (int)input.x, (int)input.y));
        }
    }

    public int GetFinalScore()
    {
        return Points - (Deaths * 10);
    }

    public void Move(Vector2 direction)
    {
        transform.Translate(direction * moveSpeed * Time.deltaTime);
    }

    public void Shoot()
    {
       GameObject bullet = bulletPool.SpawnFromPool(firePoint.position, Quaternion.identity);
       bullet.GetComponent<Bullet>().ShooterId = playerId;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "EnemyBullet")
        {
            Die();
            Destroy(other.gameObject);
        }

        if(other.gameObject.tag == "Enemy")
        {
            Die();
        }
    }

    public void Die()
    {
        NetworkClient.Instance.Send(new PlayerDiesMsg(playerId));
        gameObject.SetActive(false);
    }
}