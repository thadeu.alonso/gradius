﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIActions : MonoBehaviour
{
    public void ConectarServidor()
    {
    }

    public void EnviaMensagem(string mensagem)
    {
    }

    public void Login()
    {
        string mail = GameObject.Find("inpMail").GetComponent<TMP_InputField>().text;
        string senha = GameObject.Find("inpSenha").GetComponent<TMP_InputField>().text;

        DatabaseManager.Instance.Login(mail, senha);
    }

    public void CadastrarJogador()
    {
        string mail = GameObject.Find("inpNewMail").GetComponent<TMP_InputField>().text;
        string nick = GameObject.Find("inpNewNick").GetComponent<TMP_InputField>().text;
        string senha = GameObject.Find("inpNewSenha").GetComponent<TMP_InputField>().text;

        DatabaseManager.Instance.Cadastrar(mail, nick, senha);
    }

    public void ShowPanel(GameObject go)
    {
        UIManager.Instance.ToggleCanvasGroup(go.name, true);
    }

    public void ClosePanel(GameObject go)
    {
        UIManager.Instance.ToggleCanvasGroup(go.name, false);
    }

    public void TooglePanel(GameObject go)
    {
        CanvasGroup canvasGroup = go.GetComponent<CanvasGroup>();

        UIManager.Instance.ToggleCanvasGroup(canvasGroup);
    }

    //TODO: Alterar método para reposição de objetos na cena
    // ao inves de recarregar a cena
    public void RestartScene()
    {
        LevelManager.Instance.RestartScene();
    }

    public void LoadScene(string scene)
    {
        LevelManager.Instance.LoadScene(scene);
    }

    public void LoadScene(int sceneIndex)
    {
        LevelManager.Instance.LoadScene(sceneIndex);
    }

    public void LoadLastScene()
    {
        LevelManager.Instance.LoadLastScene();
    }
}