﻿using Gradius.DAO.model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class NetworkClient : Singleton<NetworkClient>
{
    [SerializeField] bool connectedToServer = false;
    [SerializeField] int id = -1;
    [SerializeField] string address = "127.0.0.1";
    [SerializeField] int port = 5000;

    private TcpClient tcpClient;
    private StreamReader reader;
    private StreamWriter writer;
    private Thread thread;

    public int NetworkId { get { return id; } set { id = value; } }

    public void Connect()
    {
        if (connectedToServer)
        {
            Debug.LogError("NetworkClient.Connect() :: Cliente já conectado ao servidor!");
            return;
        }

        try
        {
            tcpClient = new TcpClient(address, port);

            NetworkStream stream = tcpClient.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);

            thread = new Thread(Run);
            thread.Start();

            connectedToServer = true;
        }
        catch (SocketException e)
        {
            Debug.LogError("Erro de rede: " + e.Message);
        }
    }

    public void Disconnect()
    {
        if (connectedToServer)
        {
            tcpClient.Close();
            reader.Close();
            writer.Close();
            thread = null;
            connectedToServer = false;
            Debug.Log("Desconectado do servidor");
        }
    }

    private void Run()
    {
        try
        {
            string message = reader.ReadLine();

            while (message != null)
            {
                ConsumeMessage(message);

                message = reader.ReadLine();
            }
        }
        catch (SocketException e)
        {
            Debug.LogError("Erro de rede: " + e.Message);
            reader.Close();
            writer.Close();
            tcpClient.Close();
            thread.Abort();
        }
    }

    private void ConsumeMessage(string message)
    {
        Message msg = JsonConvert.DeserializeObject<Message>(message);
        Action action = null;

        switch (msg.MessageType)
        {
            case MessageType.GAME_STARTED:

                GameStartedMsg gameStartMsg = JsonConvert.DeserializeObject<GameStartedMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnGameStartsEvent());
                };

                break;

            case MessageType.GAME_OVER:

                GameOverMsg gameOverMsg = JsonConvert.DeserializeObject<GameOverMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnGameOverEvent());
                };

                break;

            case MessageType.PLAYER_CONNECTED:

                PlayerConnectedMsg conectedMsg = JsonConvert.DeserializeObject<PlayerConnectedMsg>(message);

                action = () =>
                {
                    if (!GameplayManager.Instance.ConnectedToServer)
                        NetworkId = conectedMsg.PlayerId;

                    EventManager.TriggerEvent(new OnPlayerConnectedEvent(conectedMsg.PlayerId, this));
                    Debug.Log("Connected to server with Id: " + NetworkId);
                };

                break;

            case MessageType.PLAYER_LOGGED:

                PlayerLoggedMsg playerLoggedMsg = JsonConvert.DeserializeObject<PlayerLoggedMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnLoginSuccessfulEvent(playerLoggedMsg.CodP1, playerLoggedMsg.CodP2));
                };

                break;

            case MessageType.PLAYER_MOVE:

                PlayerMoveMsg moveMsg = JsonConvert.DeserializeObject<PlayerMoveMsg>(message);

                action = () =>
                {
                    Vector2 direction = new Vector2(moveMsg.DirX, moveMsg.DirY);
                    EventManager.TriggerEvent(new OnPlayerMoveEvent(msg.PlayerId, direction));
                };

                break;

            case MessageType.PLAYER_SHOOT:

                PlayerShootMsg shootMsg = JsonConvert.DeserializeObject<PlayerShootMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnPlayerShootEvent(msg.PlayerId, shootMsg.ShootPosition));
                };

                break;

            case MessageType.PLAYER_DIES:

                PlayerDiesMsg playerDiesMsg = JsonConvert.DeserializeObject<PlayerDiesMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnPlayerDiesEvent(msg.PlayerId));
                };

                break;

            case MessageType.OBSTACLE_SPAWNED:

                ObstacleSpawnedMsg obstacleSpawnMsg = JsonConvert.DeserializeObject<ObstacleSpawnedMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnObstacleSpawnedEvent(obstacleSpawnMsg.ObstacleIndex, obstacleSpawnMsg.PositionIndex));
                };

                break;

            case MessageType.OBSTACLE_DESTROYED:

                ObstacleDestroyedMsg obstacleDestroyedMsg = JsonConvert.DeserializeObject<ObstacleDestroyedMsg>(message);

                action = () =>
                {
                    EventManager.TriggerEvent(new OnObstacleDestroyedEvent(obstacleDestroyedMsg.Points, obstacleDestroyedMsg.ShooterID));
                };

                break;
        }

        if(action != null)
            ThreadManager.Instance.AddAction(action);
    }

    public void Send(Message message)
    {
        string json = JsonConvert.SerializeObject(message);

        writer.WriteLine(json);
        writer.Flush();
    }
}