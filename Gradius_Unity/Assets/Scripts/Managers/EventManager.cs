﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using Gradius.DAO.model;

public class EventManager : Singleton<EventManager>
{
    private Dictionary<Type, Action<EventMessage>> eventDictionary;

    protected override void Awake()
    {
        base.Awake();

        if (eventDictionary == null)
            eventDictionary = new Dictionary<Type, Action<EventMessage>>();
    }

    public static void StartListening<MessageType>(Action<EventMessage> listener)
    {
        Action<EventMessage> thisEvent;

        if (Instance.eventDictionary.TryGetValue(typeof(MessageType), out thisEvent))
        {
            thisEvent += listener;
            Instance.eventDictionary[typeof(MessageType)] = thisEvent;
        }
        else
        {
            thisEvent += listener;
            Instance.eventDictionary.Add(typeof(MessageType), thisEvent);
        }
    }

    public static void StopListening<MessageType>(Action<EventMessage> listener)
    {
        if (Instance == null) return;

        Action<EventMessage> thisEvent;

        if (Instance.eventDictionary.TryGetValue(typeof(MessageType), out thisEvent))
        {
            thisEvent -= listener;
            Instance.eventDictionary[typeof(MessageType)] = thisEvent;
        }
    }

    public static void TriggerEvent(EventMessage message)
    {
        Action<EventMessage> thisEvent = null;

        if (Instance.eventDictionary.TryGetValue(message.GetType(), out thisEvent))
        {
            if (thisEvent == null)
                Debug.Log("nullref " + message.GetType());
            else
                thisEvent.Invoke(message);
        }
    }
}

public abstract class EventMessage { }

#region Banco de dados
public class OnLoginSuccessfulEvent : EventMessage
{
    public int CodP1 { get; set; }
    public int CodP2 { get; set; }

    public OnLoginSuccessfulEvent(int _codP1, int _codP2)
    {
        CodP1 = _codP1;
        CodP2 = _codP2;
    }
}
public class OnRegisterSuccessfulEvent : EventMessage
{
    public int CodJogador { get; set; }

    public OnRegisterSuccessfulEvent(int codJogador)
    {
        CodJogador = codJogador;
    }
}
#endregion

#region Servidor
public class OnGameStartsEvent : EventMessage { }
public class OnGameOverEvent : EventMessage { }
public class OnPlayerConnectedEvent : EventMessage
{
    public int Id { get; set; }
    public NetworkClient Client { get; set; }

    public OnPlayerConnectedEvent(int _id, NetworkClient _client)
    {
        Id = _id;
        Client = _client;
    }
}
public class OnPlayerMoveEvent : EventMessage
{
    public int PlayerId { get; set; }
    public Vector2 Input { get; set; }

    public OnPlayerMoveEvent(int _playerId, Vector2 _input)
    {
        PlayerId = _playerId;
        Input = _input;
    }
}
public class OnPlayerShootEvent : EventMessage
{
    public int PlayerId { get; set; }
    public Vector2 ShootPosition { get; set; }

    public OnPlayerShootEvent(int playerId, Vector2 shootPosition)
    {
        PlayerId = playerId;
        ShootPosition = shootPosition;
    }
}
public class OnPlayerDiesEvent : EventMessage
{
    public int PlayerId { get; set; }

    public OnPlayerDiesEvent(int playerId)
    {
        this.PlayerId = playerId;
    }
}
public class OnObstacleDestroyedEvent : EventMessage
{
    public int Points { get; set; }
    public int ShooterID { get; set; }

    public OnObstacleDestroyedEvent(int points, int shooterID)
    {
        Points = points;
        ShooterID = shooterID;
    }
}
public class OnObstacleSpawnedEvent : EventMessage
{
    public int ObstacleIndex { get; set; }
    public int PositionIndex { get; set; }

    public OnObstacleSpawnedEvent(int enemyIndex, int positionIndex)
    {
        this.ObstacleIndex = enemyIndex;
        this.PositionIndex = positionIndex;
    }
}
#endregion