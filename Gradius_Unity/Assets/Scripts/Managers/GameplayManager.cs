﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : Singleton<GameplayManager>
{
    [Header("General Settings;")]
    [SerializeField] List<GameObject> playersPrefabs;
    [SerializeField] List<Transform> spawnPositions;

    [Header("Gameplay Settings")]
    [SerializeField] int totalTime = 60;
    [SerializeField] int totalLifes = 3;

    private NetworkClient client;
    private List<Player> players;
    
    public bool ConnectedToServer { get; set; }
    public bool GameStarted { get; set; }
    public float CurrentTime { get; set; }

    private int codLastPlayer;

    #region Eventos
    void OnEnable()
    {
        EventManager.StartListening<OnLoginSuccessfulEvent>(OnLoginSuccessful);
        EventManager.StartListening<OnPlayerConnectedEvent>(OnPlayerConnect);
        EventManager.StartListening<OnGameStartsEvent>(OnGameStarts);
        EventManager.StartListening<OnGameOverEvent>(OnGameOver);
        EventManager.StartListening<OnPlayerDiesEvent>(OnPlayerDies);
        EventManager.StartListening<OnObstacleDestroyedEvent>(OnObstacleDestroyed);
    }

    void OnDisable()
    {
        EventManager.StopListening<OnLoginSuccessfulEvent>(OnLoginSuccessful);
        EventManager.StopListening<OnPlayerConnectedEvent>(OnPlayerConnect);
        EventManager.StopListening<OnGameStartsEvent>(OnGameStarts);
        EventManager.StartListening<OnGameOverEvent>(OnGameOver);
        EventManager.StopListening<OnPlayerDiesEvent>(OnPlayerDies);
        EventManager.StartListening<OnObstacleDestroyedEvent>(OnObstacleDestroyed);
    }

    private void OnLoginSuccessful(EventMessage obj)
    {
        OnLoginSuccessfulEvent msg = (OnLoginSuccessfulEvent)obj;

        players[0].codJogador = msg.CodP1;
        players[1].codJogador = msg.CodP2;

        UIManager.Instance.SetTextUIElement("txtCod", msg.CodP1 + "/" + msg.CodP2);
    }

    private void OnPlayerConnect(EventMessage obj)
    {
        OnPlayerConnectedEvent msg = (OnPlayerConnectedEvent)obj;

        int remotePlayerID = msg.Id == 0 ? 1 : 0;

        if (players == null)
        {
            players = new List<Player>();
            InstantiatePlayers();
        }

        players[msg.Id].IsLocalPlayer = true;
        players[msg.Id].PlayerId = msg.Id;
        players[remotePlayerID].PlayerId = remotePlayerID;
        ConnectedToServer = true;

        UIManager.Instance.SetTextUIElement("txtId", players[msg.Id].PlayerId.ToString());
    }

    private void OnGameStarts(EventMessage obj)
    {
        UIManager.Instance.ToggleCanvasGroup("Login", false);
        StartGame();
    }

    private void OnGameOver(EventMessage obj)
    {
        FinishGame();

        PartidaInfo partidaInfo = new PartidaInfo();

        partidaInfo.codP1 = players[0].codJogador;
        partidaInfo.codP2 = players[1].codJogador;
        partidaInfo.mortesP1 = players[0].Deaths;
        partidaInfo.mortesP2 = players[1].Deaths;

        DatabaseManager.Instance.RegistrarPartida(partidaInfo);
    }

    private void OnPlayerDies(EventMessage obj)
    {
        OnPlayerDiesEvent msg = (OnPlayerDiesEvent)obj;

        Player player = players[msg.PlayerId];

        player.Lifes--;
        player.Deaths++;

        if(player.Lifes > 0)
        {
            StartCoroutine(CoRespawn(player.PlayerId));
        }
        else
        {
            player.IsDead = true;
            Debug.Log("P" + player.PlayerId + " morreu!");
        }
    }

    private IEnumerator CoRespawn(int playerId)
    {
        yield return new WaitForSeconds(2f);
        players[playerId].transform.position = spawnPositions[playerId].position;
        players[playerId].gameObject.SetActive(true);
    }

    private void OnObstacleDestroyed(EventMessage obj)
    {
        OnObstacleDestroyedEvent msg = (OnObstacleDestroyedEvent)obj;

        AddPoints(msg.ShooterID, msg.Points);
    }
    #endregion

    void Start()
    {
        client = GetComponent<NetworkClient>();
        players = new List<Player>();

        InstantiatePlayers();
    }

    void Update()
    {
        if (GameStarted)
        {
            if(AllPlayersDead())
                client.Send(new GameOverMsg());

            if(CurrentTime <= totalTime)
            {
                CurrentTime += Time.deltaTime;
                UIManager.Instance.SetTextUIElement("txtTime", Mathf.RoundToInt(CurrentTime).ToString());
            }
            else
            {
                FinishGame();
                client.Send(new GameOverMsg());
            }
        }
    }

    private bool AllPlayersDead()
    {
        foreach (Player player in players)
        {
            if (!player.IsDead)
                return false;
        }

        return true;
    }

    public void InstantiatePlayers()
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject playerGO = Instantiate(playersPrefabs[i], spawnPositions[i].position, Quaternion.identity);
            Player player = playerGO.GetComponent<Player>();

            players.Add(player);
        }
    }

    public void StartGame()
    {
        GameStarted = true;

        for (int i = 0; i < 2; i++)
        {
            UIManager.Instance.SetTextUIElement("txtP" + i + "Score", players[i].Points.ToString());
        }
    }

    public void FinishGame()
    {
        GameStarted = false;
        UIManager.Instance.ToggleCanvasGroup("pnlGameOver", true);

        for (int i = 0; i < 2; i++)
        {
            UIManager.Instance.SetTextUIElement("txtP" + i + "FinalScore", players[i].GetFinalScore().ToString());
        }
    }

    public int GetCurrentID()
    {
        return client.NetworkId;
    }

    public void AddPoints(int shooterID, int points)
    {
        players[shooterID].Points += points;
        Debug.Log("P" + shooterID + " ganhou " + points + " pontos. Total: " + players[shooterID].Points);

        for (int i = 0; i < 2; i++)
        {
            UIManager.Instance.SetTextUIElement("txtP" + i + "Score", players[i].Points.ToString());
        }
    }
}