﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadManager : Singleton<ThreadManager>
{
    public Queue<Action> ThreadActions { get; set; }

    protected override void Awake()
    {
        base.Awake();

        ThreadActions = new Queue<Action>();
    }

    private void Update()
    {
        if(ThreadActions.Count > 0)
        {
            Action action = ThreadActions.Dequeue();
            action();
        }
    }

    public void AddAction(Action action)
    {
        ThreadActions.Enqueue(action);
    }
}