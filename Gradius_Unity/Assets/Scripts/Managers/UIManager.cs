using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : Singleton<UIManager>
{
    public void UpdateStatusBar(string elementName, int currentValue, int maxValue)
    {
        GameObject bar = GameObject.Find(elementName);

        if (bar != null)
        {
            bar.GetComponent<Slider>().value = currentValue;
            bar.GetComponent<Slider>().maxValue = maxValue;

            if (bar.GetComponentInChildren<Text>() != null)
                bar.GetComponentInChildren<Text>().text = currentValue + " / " + maxValue;
        }
    }

    public void SetMaxValueBar(string elementName, int value)
    {
        Slider bar = GameObject.Find(elementName).GetComponent<Slider>();
        bar.maxValue = value;
    }

    public void ToggleCanvasGroup(CanvasGroup element)
    {
        CanvasGroup group = element;

        group.blocksRaycasts = !group.blocksRaycasts;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(CanvasGroup element, bool active)
    {
        CanvasGroup group = element;

        group.blocksRaycasts = active;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(string element)
    {
        CanvasGroup group = GameObject.Find(element).GetComponent<CanvasGroup>();

        group.blocksRaycasts = !group.blocksRaycasts;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(string element, bool active)
    {
        if(GameObject.Find(element) && GameObject.Find(element).GetComponent<CanvasGroup>())
        {
            CanvasGroup group = GameObject.Find(element).GetComponent<CanvasGroup>();

            group.blocksRaycasts = active;
            group.alpha = group.blocksRaycasts ? 1 : 0;
        }
    }

    public Toggle GetToogleElement(string toggleName)
    {
        return GameObject.Find(toggleName).GetComponent<Toggle>();
    }

    public Button GetButtonByName(string btnName)
    {
        if(GameObject.Find(btnName) != null)
            return GameObject.Find(btnName).GetComponent<Button>();
        return null;
    }

    public void SetTextUIElement(string elementName, string text)
    {
        TextMeshProUGUI normalText = GameObject.Find(elementName).GetComponentInChildren<TextMeshProUGUI>();

        normalText.text = text;
    }

    public void ToggleImage(string imageName)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.enabled = !image.enabled;
    }

    public void ToggleImage(string imageName, bool active)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.enabled = active;
    }

    public void SetImageColor(string imageName, Color color)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.color = color;
    }
}