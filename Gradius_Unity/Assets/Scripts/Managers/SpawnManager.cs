﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : Singleton<SpawnManager>
{
    [SerializeField] float spawnDelay;
    private float spawnTimer;
    [SerializeField] List<GameObject> obstacles;
    [SerializeField] List<Transform> spawnPositions;

    public bool GameStarted { get; set; }

    void OnEnable()
    {
        EventManager.StartListening<OnGameStartsEvent>(OnGameStarts);
        EventManager.StartListening<OnGameOverEvent>(OnGameOver);
        EventManager.StartListening<OnObstacleSpawnedEvent>(OnObstacleSpawned);
    }

    void OnDisable()
    {
        EventManager.StopListening<OnGameStartsEvent>(OnGameStarts);
        EventManager.StopListening<OnGameOverEvent>(OnGameOver);
        EventManager.StopListening<OnObstacleSpawnedEvent>(OnObstacleSpawned);
    }

    private void OnGameStarts(EventMessage obj)
    {
        GameStarted = true;
    }

    private void OnGameOver(EventMessage obj)
    {
        GameStarted = false;
    }

    private void OnObstacleSpawned(EventMessage obj)
    {
        OnObstacleSpawnedEvent msg = (OnObstacleSpawnedEvent)obj;

        CreateObstacle(msg.ObstacleIndex, msg.PositionIndex);
    }

    private void Update()
    {
        if (GameStarted)
        {
            if (GameplayManager.Instance.GetCurrentID() != 0)
                return;

            if (spawnTimer <= 0f)
            {
                SpawnObstacle();
                spawnTimer = spawnDelay;
            }
            else
            {
                spawnTimer -= Time.deltaTime;
            }
        }
    }

    private void SpawnObstacle()
    {
        for (int i = 0; i < obstacles.Count; i++)
        {
            Obstacle obstacle = obstacles[i].GetComponent<Obstacle>();
            int currentTime = Mathf.RoundToInt(GameplayManager.Instance.CurrentTime);

            if (obstacle.CanSpawnInTime(currentTime))
            {
                int randPos;

                if (obstacle is Enemy)
                    randPos = UnityEngine.Random.Range(0, spawnPositions.Count);
                else
                    randPos = UnityEngine.Random.Range(3, 5);

                CreateObstacle(i, randPos);
                NetworkClient.Instance.Send(new ObstacleSpawnedMsg(i, randPos));
            }
        }
    }

    private void CreateObstacle(int obstacleIndex, int positionIndex)
    {
        GameObject obstacle = Instantiate(obstacles[obstacleIndex], spawnPositions[positionIndex].position, Quaternion.identity);

        if (positionIndex == 4)
            obstacle.GetComponent<SpriteRenderer>().flipY = true;
    }
}