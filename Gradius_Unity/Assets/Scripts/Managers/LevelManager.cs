using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class LevelManager : Singleton<LevelManager>
{
    public string CurrentScene { get; set; }
    public string LastScene { get; set; }
    public int CurrentBuildIndex { get; set; }
    public int LastBuildIndex { get; set; }
    public bool IsLoadingScene { get; set; }
 

    public void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelLoaded;
    }

    private void OnLevelLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        CurrentScene        = SceneManager.GetActiveScene().name;
        CurrentBuildIndex   = SceneManager.GetActiveScene().buildIndex;
    }

    public void RestartScene()
    {
        CurrentBuildIndex = SceneManager.GetActiveScene().buildIndex;

        LoadScene(CurrentBuildIndex);
    }

    public void LoadLastScene()
    {
        if (!string.IsNullOrEmpty(LastScene))
            LoadScene(LastScene);
        else
            LoadScene(0);
    }

    public void LoadScene(string scene)
    {
        SaveLastScene();
        IsLoadingScene = true;

        SceneManager.LoadScene(scene);
    }

    public void LoadScene(int sceneIndex)
    {
        SaveLastScene();
        IsLoadingScene = true;

        SceneManager.LoadScene(sceneIndex);
    }
    
    private void SaveLastScene()
    {
        LastScene = CurrentScene;
        LastBuildIndex = CurrentBuildIndex;
    }

    public void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelLoaded;
    }
}