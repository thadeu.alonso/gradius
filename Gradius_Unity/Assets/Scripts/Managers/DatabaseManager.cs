﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Gradius.DAO.model;
using Gradius.DAO.controller;

public class DatabaseManager : Singleton<DatabaseManager>
{
    public void Login(string mail, string senha)
    {
        try
        {
            Jogador jogador = new CtrlJogador().login(mail, senha);

            if (jogador != null)
            {
                UIManager.Instance.SetTextUIElement("lblDebug", "Login realizado com sucesso!");

                NetworkClient.Instance.Connect();

                int id = NetworkClient.Instance.NetworkId;

                if (id == 0)
                {
                    NetworkClient.Instance.Send(new PlayerLoggedMsg(jogador.cod, 0));
                }
                else
                {
                    NetworkClient.Instance.Send(new PlayerLoggedMsg(0, jogador.cod));
                }
                UIManager.Instance.ToggleCanvasGroup("pnlLobby", true);
            }
        }
        catch (System.Exception e)
        {
            UIManager.Instance.SetTextUIElement("lblDebug", "Erro de Login: " + e.Message);
        }
    }

    public void Cadastrar(string mail, string nick, string senha)
    {
        try
        {
            Jogador jogador = new Jogador();

            jogador.mail = mail;
            jogador.nick = nick;
            jogador.senha = senha;

            new CtrlJogador().inserir(jogador);

            UIManager.Instance.SetTextUIElement("lblDebug", "Cadastro realizado com sucesso!");
            EventManager.TriggerEvent(new OnRegisterSuccessfulEvent(jogador.cod));
        }
        catch (System.Exception e)
        {
            UIManager.Instance.SetTextUIElement("lblDebug", "Erro de Cadastro: " + e.Message);
        }
    }

    public void RegistrarPartida(PartidaInfo info)
    {
        try
        {
            Partida newPartida = new Partida();

            newPartida.jogador1 = new CtrlJogador().buscarPorCod(info.codP1);
            newPartida.jogador2 = new CtrlJogador().buscarPorCod(info.codP2);
            newPartida.morte1 = info.mortesP1;
            newPartida.morte2 = info.mortesP2;

            new CtrlPartida().inserir(newPartida);
        }
        catch (System.Exception e)
        {
            UIManager.Instance.SetTextUIElement("lblDebug", "Erro: " + e.Message);
        }
    }
}

[System.Serializable]
public class PartidaInfo
{
    public int codP1;
    public int codP2;
    public int mortesP1;
    public int mortesP2;
}