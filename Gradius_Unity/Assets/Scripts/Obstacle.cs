﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [Header("Gameplay Settings")]
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] int life = 1;
    [SerializeField] int damage = 1;
    [SerializeField] int points;

    [Header("Spawn Settings")]
    [SerializeField] int interval;

    public int Interval { get { return interval; } set { interval = value; } }

    public void Setup(int minTime)
    {
        interval = minTime;
    }

    protected virtual void Update()
    {
        transform.Translate(-Vector2.right * moveSpeed * Time.deltaTime);
    }

    public bool CanSpawnInTime(int currentTime)
    {
        return currentTime >= interval;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            int shooterID = other.GetComponent<Bullet>().ShooterId;
            Die(shooterID);
        }

        if(other.gameObject.tag == "GarbageCollector")
        {
            Destroy(gameObject);
        }
    }

    private void Die(int shooterID)
    {
        NetworkClient.Instance.Send(new ObstacleDestroyedMsg(shooterID, points));
        Destroy(gameObject);
    }
}