﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] int poolSize = 10;

    private Queue<GameObject> queueObjects;

    private void Start()
    {
        queueObjects = new Queue<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject obj = Instantiate(prefab);

            obj.SetActive(false);
            queueObjects.Enqueue(obj);
        }
    }

    public GameObject SpawnFromPool(Vector2 position, Quaternion rotation)
    {
        GameObject objectToSpawn = queueObjects.Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        queueObjects.Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}