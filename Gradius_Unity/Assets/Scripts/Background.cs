﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] float backgroundSpeed = 5f;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        Vector2 offset = new Vector2(backgroundSpeed * Time.deltaTime, 0f);
        meshRenderer.material.mainTextureOffset += offset;
    }
}
