﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace videoaulas
{
    public class Cliente : MonoBehaviour
    {
        [SerializeField] string servidor = "127.0.0.1";

        TcpClient cliente;
        StreamReader reader;
        StreamWriter writer;
        Thread thread;

        public void conectar()
        {
            try
            {
                if(cliente != null)
                    return;

                int porta = 5000;
                cliente = new TcpClient(servidor, porta);
                NetworkStream stream = cliente.GetStream();
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);

                thread = new Thread(listening);
                thread.Start();
            }
            catch (System.Exception e)
            {
                Debug.Log("Erro no cliente: " + e.Message);
            }
        }

        public void enviar(string mensagem)
        {
            writer.WriteLine(mensagem);
            writer.Flush();
        }

        private void listening()
        {
            try
            {
                string dados = null;
                dados = reader.ReadLine();

                while (dados != null)
                {
                    processaMensagem(dados);
                    dados = reader.ReadLine();
                }
            }
            catch (System.Exception e)
            {
                Debug.Log("Erro no cliente: " + e.Message);
            }
        }

        private void processaMensagem(string dados)
        {
        }
    }
}