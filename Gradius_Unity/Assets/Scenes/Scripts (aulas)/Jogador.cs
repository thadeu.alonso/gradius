﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace videoaulas
{
    public class Jogador : MonoBehaviour
    {
        [Header("Movimento")]
        [SerializeField] float moveSpeed = 5f;

        Cliente cliente;

        void Awake()
        {
            cliente = GetComponent<Cliente>();
            cliente.conectar();
        }

        void Update()
        {
            float vertical = Input.GetAxisRaw("Vertical");
            float horizontal = Input.GetAxisRaw("Horizontal");

            if (horizontal != 0f || vertical != 0f)
            {
                Move(new Vector2(horizontal, vertical));
            }
        }

        private void Move(Vector2 direction)
        {
            cliente.enviar("moveu");
            transform.Translate(direction * moveSpeed * Time.deltaTime);
        }
    }
}