﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Gradius.DAO.model;
using Gradius.DAO.controller;

namespace Gradius.View
{
    public partial class TesteInsert : System.Web.UI.Page
    {
        Jogador jogador;
        CtrlJogador ctrlJogador;

        protected void Page_Load(object sender, EventArgs e)
        {
            atualizarGrid();
            atualizarRanking();
        }

        private void atualizarGrid()
        {
            jogador = new Jogador();
            ctrlJogador = new CtrlJogador();

            grdJogadores.DataSource = ctrlJogador.listar();
            grdJogadores.DataBind();
        }

        private void atualizarRanking()
        {
            List<Ranking> ranking = new CtrlEstatistica().ranking();

            grdRanking.DataSource = ranking;
            grdRanking.DataBind();
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            int cod = Int32.Parse(txtCod.Text);

            jogador = ctrlJogador.buscarPorCod(cod);

            if (jogador != null)
            {

                lblError.Text = "Jogador já cadastrado!";
                limparCampos();
            }
            else
            {
                jogador = new Jogador();
                jogador.mail = txtMail.Text;
                jogador.nick = txtNick.Text;
                jogador.senha = txtSenha.Text;

                try
                {
                    ctrlJogador.inserir(jogador);
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                }
                finally
                {
                    limparCampos();
                }
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            int cod = Int32.Parse(txtCod.Text);

            jogador = ctrlJogador.buscarPorCod(cod);

            if(jogador != null)
            {
                jogador.mail = txtMail.Text;
                jogador.nick = txtNick.Text;
                jogador.senha = txtSenha.Text;

                try
                {
                    ctrlJogador.alterar(jogador);
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                }
                finally
                {
                    limparCampos();
                }
            }
        }

        protected void btnExcluir_Click(object sender, EventArgs e)
        {
            int cod = Int32.Parse(txtCod.Text);

            jogador = ctrlJogador.buscarPorCod(cod);

            if (jogador != null)
            {
                try
                {
                    ctrlJogador.excluir(jogador);
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                }
                finally
                {
                    limparCampos();
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            int cod = Int32.Parse(txtCod.Text);

            jogador = ctrlJogador.buscarPorCod(cod);

            if (jogador != null)
            {
                txtCod.Text = jogador.cod.ToString();
                txtMail.Text = jogador.mail;
                txtNick.Text = jogador.nick;
                txtSenha.Text = jogador.senha;
            }
            else
            {
                txtMail.Text = "NÃO ENCONTRADO";
                txtNick.Text = "NÃO ENCONTRADO";
                txtSenha.Text = "NÃO ENCONTRADO";
            }
        }

        private void limparCampos()
        {
            txtCod.Text = "";
            txtMail.Text = "";
            txtNick.Text = "";
            txtSenha.Text = "";
        }
    }
}