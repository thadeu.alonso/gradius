﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As Informações Gerais sobre um assembly são controladas por meio do 
// conjunto de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("Gradius.View")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Gradius.View")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Configurar o ComVisible como false torna os tipos desse assembly invisíveis 
// para componentes COM.  Se for necessário acessar um tipo nesse assembly a partir do 
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// A GUID a seguir será referente à ID do typelib se este projeto for exposto ao COM
[assembly: Guid("e0b95e1d-3a93-425d-94eb-946ba6961756")]

// As informações de versão de um assembly consistem nos quatro valores a seguir:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// É possível especificar todos os valores ou utilizar como padrão os Números de Revisão e da Versão 
// usando o '*' como mostrado abaixo:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
