﻿using Gradius.DAO.controller;
using Gradius.DAO.model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Gradius.View
{
    public partial class Login : System.Web.UI.Page
    {
        Jogador jogador;
        CtrlJogador cJogador;

        protected void Page_Load(object sender, EventArgs e)
        {
            cJogador = new CtrlJogador();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string nick = txtLoginNick.Text;
            string senha = txtLoginSenha.Text;

            try
            {
                jogador = cJogador.login(nick, senha);
            }
            catch(Exception exc)
            {
                lblOutput.Text = exc.Message;
            }

            if(jogador != null)
            {
                lblOutput.Text = "Login efetuado com sucesso!\n"
                    + "Email: " + jogador.mail;
            }
        }
    }
}