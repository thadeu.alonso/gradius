﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gradius.DAO.model
{
    public class Estatistica
    {
        public int cod { get; set; }
        public Partida partida { get; set; }
        public Obstaculo obstaculo { get; set; }
        public int vida { get; set; }
        public Jogador assassino { get; set; }
    }
}