﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gradius.DAO.model
{
    public class Obstaculo
    {
        public int cod { get; set; }
        public String nome { get; set; }
        public int vida { get; set; }
        public int ponto { get; set; }
        public int intervalo { get; set; }
    }
}