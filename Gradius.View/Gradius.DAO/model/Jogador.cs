﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gradius.DAO.model
{
    public class Jogador
    {
        public int cod { get; set; }
        public String mail { get; set; }
        public String nick { get; set; }
        public String senha { get; set; }
    }
}