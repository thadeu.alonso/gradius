﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gradius.DAO.model
{
    public class Partida
    {
        public int cod { get; set; }
        public int morte1 { get; set; }
        public int morte2 { get; set; }
        public Jogador jogador1 { get; set; }
        public Jogador jogador2 { get; set; }
    }
}