﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gradius.DAO.model
{
    public class Ranking
    {
        public int cod { get; set; }
        public String nick { get; set; }
        public int ponto { get; set; }
    }
}