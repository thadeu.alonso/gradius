﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using System.Configuration;

namespace Gradius.DAO.controller
{
    public class DAO
    {
        public SqlParameter par;
        public SqlCommand command;
        public SqlDataReader reader;
        public SqlConnection conn;

        public SqlConnection conectar()
        {
            String strConexao = ConfigurationManager.ConnectionStrings["Banco"].ConnectionString;

            conn = new SqlConnection(strConexao);

            try
            {
                conn.Open();
            }
            catch (SqlException e)
            {
                throw e;
            }
            
            return conn;
        }
    }
}