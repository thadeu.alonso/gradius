﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Gradius.DAO.model;
using System.Data.SqlClient;

namespace Gradius.DAO.controller
{
    public class CtrlJogador : DAO
    {
        #region CRUD

        public void inserir(Jogador jogador)
        {
            String sql = "INSERT INTO JOGADOR (MAIL, NICK, SENHA) VALUES (@MAIL, @NICK, @SENHA);";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@MAIL", jogador.mail);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@NICK", jogador.nick);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@SENHA", jogador.senha);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void alterar(Jogador jogador)
        {
            String sql = "UPDATE JOGADOR SET MAIL = @MAIL, NICK = @NICK, SENHA = @SENHA "
                + "WHERE COD = @COD";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@MAIL", jogador.mail);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@NICK", jogador.nick);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@SENHA", jogador.senha);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@COD", jogador.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void excluir(Jogador jogador)
        {
            String sql = "DELETE FROM JOGADOR WHERE COD = @COD;";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", jogador.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region CONSULTAS

        public Jogador buscarPorCod(int cod)
        {
            String sql = "SELECT COD, MAIL, NICK, SENHA FROM JOGADOR WHERE COD = @COD;";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                reader = command.ExecuteReader();

                if(reader.Read())
                {
                    Jogador j = new Jogador();

                    j.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    j.mail = reader.GetValue(reader.GetOrdinal("MAIL")).ToString();
                    j.nick = reader.GetValue(reader.GetOrdinal("NICK")).ToString();
                    j.senha = reader.GetValue(reader.GetOrdinal("SENHA")).ToString();
                    return j;
                }
            }
            finally
            {
                conn.Close();
            }

            return null;
        }

        public List<Jogador> listar()
        {
            String sql = "SELECT COD, MAIL, NICK, SENHA FROM JOGADOR ORDER BY NICK;";

            command = new SqlCommand(sql, conectar());
            List<Jogador> jogadores = new List<Jogador>();

            try
            {
                reader = command.ExecuteReader();

                Jogador j;

                while (reader.Read())
                {
                    j = new Jogador();
                    j.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    j.mail = reader.GetValue(reader.GetOrdinal("MAIL")).ToString();
                    j.nick = reader.GetValue(reader.GetOrdinal("NICK")).ToString();
                    j.senha = reader.GetValue(reader.GetOrdinal("SENHA")).ToString();

                    jogadores.Add(j);
                }

            }
            finally
            {
                conn.Close();
            }

            return jogadores;
        }

        public Jogador login(string mail, string senha)
        {
            string sql = "SELECT COD, MAIL, NICK, SENHA FROM JOGADOR WHERE MAIL = @MAIL;";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@MAIL", mail);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            Jogador j;

            try
            {
                reader = command.ExecuteReader();

                if(reader.Read())
                {
                    string senhaBanco = reader.GetValue(reader.GetOrdinal("SENHA")).ToString();

                    if(senha.Equals(senhaBanco))
                    {
                        j = new Jogador();
                        j.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                        j.mail = reader.GetValue(reader.GetOrdinal("MAIL")).ToString();
                        j.nick = reader.GetValue(reader.GetOrdinal("NICK")).ToString();
                        j.senha = reader.GetValue(reader.GetOrdinal("SENHA")).ToString();
                        return j;
                    }
                    else
                    {
                        throw new Exception("Senha de usuário incorreta!");
                    }
                }
            }
            catch(SqlException e)
            {
                throw e;
            }
            finally
            {
                conn.Close();
            }

            return null;
        }

        #endregion
    }
}