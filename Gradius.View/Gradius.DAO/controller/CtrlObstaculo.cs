﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Gradius.DAO.model;
using System.Data.SqlClient;

namespace Gradius.DAO.controller
{
    public class CtrlObstaculo : DAO
    {
        #region CRUD

        public void inserir(Obstaculo obstaculo)
        {
            String sql = "INSERT INTO OBSTACULO (NOME, VIDA, PONTO, INTERVALO) VALUES (@NOME, @VIDA, @PONTO, @INTERVALO);";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@NOME", obstaculo.nome);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@VIDA", obstaculo.vida);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@PONTO", obstaculo.ponto);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@INTERVALO", obstaculo.intervalo);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void alterar(Obstaculo obstaculo)
        {
            String sql = "UPDATE OBSTACULO SET NOME = @NOME, VIDA = @VIDA, PONTO = @PONTO, INTERVALO = @INTERVALO "
                + "WHERE COD = @COD";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", obstaculo.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@NOME", obstaculo.nome);
            par.SqlDbType = System.Data.SqlDbType.VarChar;
            command.Parameters.Add(par);

            par = new SqlParameter("@VIDA", obstaculo.vida);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@PONTO", obstaculo.ponto);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@INTERVALO", obstaculo.intervalo);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void excluir(Obstaculo obstaculo)
        {
            String sql = "DELETE FROM OBSTACULO WHERE COD = @COD;";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", obstaculo.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region CONSULTAS

        public Obstaculo buscarPorCod(int cod)
        {
            String sql = "SELECT COD, NOME, VIDA, PONTO, INTERVALO FROM OBSTACULO WHERE COD = @COD;";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    Obstaculo o = new Obstaculo();
                    o.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    o.nome = reader.GetValue(reader.GetOrdinal("NOME")).ToString();
                    o.vida = Int32.Parse(reader.GetValue(reader.GetOrdinal("VIDA")).ToString());
                    o.ponto = Int32.Parse(reader.GetValue(reader.GetOrdinal("PONTO")).ToString());
                    o.intervalo = Int32.Parse(reader.GetValue(reader.GetOrdinal("INTERVALO")).ToString());
                    return o;
                }
            }
            finally
            {
                conn.Close();
            }

            return null;
        }

        public List<Obstaculo> sortearObstaculos(int intervalo)
        {
            String sql = "SELECT TOP(1) NOME, VIDA, PONTO, INTERVALO FROM OBSTACULO " 
                + "WHERE INTERVALO = @INTERVALO ORDER BY NEWID();";

            command = new SqlCommand(sql, conectar());
            List<Obstaculo> obstaculos = new List<Obstaculo>();

            try
            {
                reader = command.ExecuteReader();

                par = new SqlParameter("@INTERVALO", intervalo);
                par.SqlDbType = System.Data.SqlDbType.Int;
                command.Parameters.Add(par);

                Obstaculo o;

                while (reader.Read())
                {
                    o = new Obstaculo();

                    o.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    o.nome = reader.GetValue(reader.GetOrdinal("NOME")).ToString();
                    o.vida = Int32.Parse(reader.GetValue(reader.GetOrdinal("VIDA")).ToString());
                    o.ponto = Int32.Parse(reader.GetValue(reader.GetOrdinal("PONTO")).ToString());
                    o.intervalo = Int32.Parse(reader.GetValue(reader.GetOrdinal("INTERVALO")).ToString());

                    obstaculos.Add(o);
                }
            }
            finally
            {
                conn.Close();
            }

            return obstaculos;
        }

        #endregion
    }
}