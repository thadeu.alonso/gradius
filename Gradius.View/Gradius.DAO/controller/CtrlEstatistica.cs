﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Gradius.DAO.model;
using System.Data.SqlClient;

namespace Gradius.DAO.controller
{
    public class CtrlEstatistica : DAO
    {
        #region CRUD

        public void inserir(Estatistica estatistica)
        {
            String sql = "INSERT INTO ESTATISTICA (PARTIDA, OBSTACULO, VIDA, ASSASSINO) VALUES (@PARTIDA, @OBSTACULO, @VIDA, @ASSASSINO);";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@PARTIDA", estatistica.partida.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@OBSTACULO", estatistica.obstaculo.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@VIDA", estatistica.vida);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@ASSASSINO", estatistica.assassino.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            { 
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void alterar(Estatistica estatistica)
        {
            String sql = "UPDATE ESTATISTICA SET PARTIDA = @PARTIDA, OBSTACULO = @OBSTACULO, VIDA = @VIDA, ASSASSINO = @ASSASSINO "
                + "WHERE COD = @COD";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", estatistica.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@PARTIDA", estatistica.partida.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@OBSTACULO", estatistica.obstaculo.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@VIDA", estatistica.vida);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@ASSASSINO", estatistica.assassino.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }           
            finally
            {
                conn.Close();
            }
        }

        public void excluir(Estatistica estatistica)
        {
            String sql = "DELETE FROM ESTATISTICA WHERE COD = @COD;";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", estatistica.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region CONSULTAS

        public Estatistica buscarPorCod(int cod)
        {
            String sql = "SELECT COD, PARTIDA, OBSTACULO, VIDA, ASSASSINO FROM ESTATISTICA WHERE COD = @COD;";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            reader = command.ExecuteReader();

            if (reader.Read())
            {
                Estatistica e = new Estatistica();
                CtrlPartida cPartida = new CtrlPartida();
                CtrlObstaculo cObstaculo = new CtrlObstaculo();
                CtrlJogador cJogador = new CtrlJogador();

                int codP = Int32.Parse(reader.GetValue(reader.GetOrdinal("PARTIDA")).ToString());
                int codO = Int32.Parse(reader.GetValue(reader.GetOrdinal("OBSTACULO")).ToString());
                int codJ = Int32.Parse(reader.GetValue(reader.GetOrdinal("ASSASSINO")).ToString());

                e.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                e.partida = cPartida.buscarPorCod(codP);
                e.obstaculo = cObstaculo.buscarPorCod(codO);
                e.vida = Int32.Parse(reader.GetValue(reader.GetOrdinal("VIDA")).ToString());
                e.assassino = cJogador.buscarPorCod(codJ);
                
                return e;
            }

            conn.Close();

            return null;
        }

        public List<Estatistica> listar()
        {
            String sql = "SELECT COD, PARTIDA, OBSTACULO, VIDA, ASSASSINO FROM ESTATISTICA;";

            command = new SqlCommand(sql, conectar());

            List<Estatistica> estatisticas = new List<Estatistica>();

            try
            {
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Estatistica e = new Estatistica();
                    CtrlPartida cPartida = new CtrlPartida();
                    CtrlObstaculo cObstaculo = new CtrlObstaculo();
                    CtrlJogador cJogador = new CtrlJogador();

                    int codP = Int32.Parse(reader.GetOrdinal("PARTIDA").ToString());
                    int codO = Int32.Parse(reader.GetOrdinal("OBSTACULO").ToString());
                    int codJ = Int32.Parse(reader.GetOrdinal("ASSASSINO").ToString());

                    e.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    e.partida = cPartida.buscarPorCod(codP);
                    e.obstaculo = cObstaculo.buscarPorCod(codO);
                    e.vida = Int32.Parse(reader.GetValue(reader.GetOrdinal("VIDA")).ToString());
                    e.assassino = cJogador.buscarPorCod(codJ);

                    estatisticas.Add(e);
                }
            }
            finally
            {
                conn.Close();
            }

            return estatisticas;
        }

        public List<Ranking> ranking()
        {
            String sql = "SELECT TOP(3) ASSASSINO, NICK, SUM(PONTO) AS RANKING " +
                "FROM V_PONTUACAO " +
                "GROUP BY ASSASSINO, NICK " +
                "ORDER BY RANKING DESC;";

            command = new SqlCommand(sql, conectar());

            List<Ranking> rankings = new List<Ranking>();

            try
            {
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Ranking r = new Ranking();

                    r.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("ASSASSINO")).ToString());
                    r.nick = reader.GetValue(reader.GetOrdinal("NICK")).ToString();
                    r.ponto = Int32.Parse(reader.GetValue(reader.GetOrdinal("RANKING")).ToString());

                    rankings.Add(r);
                }
            }
            finally
            {
                conn.Close();
            }

            return rankings;
        }


        #endregion
    }
}