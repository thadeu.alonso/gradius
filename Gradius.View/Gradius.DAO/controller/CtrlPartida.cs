﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Gradius.DAO.model;
using System.Data.SqlClient;

namespace Gradius.DAO.controller
{
    public class CtrlPartida : DAO
    {
        #region CRUD

        public void inserir(Partida partida)
        {
            String sql = "INSERT INTO PARTIDA (JOGADOR1, JOGADOR2, MORTE1, MORTE2) VALUES (@JOGADOR1, @JOGADOR2, @MORTE1, @MORTE2);";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@JOGADOR1", partida.jogador1.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@JOGADOR2", partida.jogador2.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@MORTE1", partida.morte1);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@MORTE2", partida.morte2);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void alterar(Partida partida)
        {
            String sql = "UPDATE PARTIDA SET JOGADOR1 = @JOGADOR1, JOGADOR2 = @JOGADOR2, MORTE1 = @MORTE1, MORTE2 = @MORTE2 "
                + "WHERE COD = @COD";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@JOGADOR1", partida.jogador1.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@JOGADOR2", partida.jogador2.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@MORTE1", partida.morte1);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            par = new SqlParameter("@MORTE2", partida.morte2);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void excluir(Partida partida)
        {
            String sql = "DELETE FROM PARTIDA WHERE COD = @COD;";
            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", partida.cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region CONSULTAS

        public Partida buscarPorCod(int cod)
        {
            String sql = "SELECT COD, JOGADOR1, JOGADOR2, MORTE1, MORTE2 FROM PARTIDA WHERE COD = @COD;";

            command = new SqlCommand(sql, conectar());

            par = new SqlParameter("@COD", cod);
            par.SqlDbType = System.Data.SqlDbType.Int;
            command.Parameters.Add(par);

            try
            {
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    Partida p = new Partida();
                    CtrlJogador cJ = new CtrlJogador();

                    int codJ1 = Int32.Parse(reader.GetValue(reader.GetOrdinal("JOGADOR1")).ToString());
                    int codJ2 = Int32.Parse(reader.GetValue(reader.GetOrdinal("JOGADOR2")).ToString());

                    p.cod = Int32.Parse(reader.GetValue(reader.GetOrdinal("COD")).ToString());
                    p.jogador1 = cJ.buscarPorCod(codJ1);
                    p.jogador2 = cJ.buscarPorCod(codJ2);
                    p.morte1 = Int32.Parse(reader.GetValue(reader.GetOrdinal("MORTE1")).ToString());
                    p.morte2 = Int32.Parse(reader.GetValue(reader.GetOrdinal("MORTE2")).ToString());
                    return p;
                }
            }
            finally
            {
                conn.Close();
            }

            return null;
        }

        #endregion
    }
}