﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using Gradius.DAO.controller;
using Gradius.DAO.model;

namespace Gradius.WS
{
    /// <summary>
    /// Descrição resumida de WS_Jogador
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que esse serviço da web seja chamado a partir do script, usando ASP.NET AJAX, remova os comentários da linha a seguir. 
    // [System.Web.Script.Services.ScriptService]
    public class WS_Jogador : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Olá, Mundo";
        }

        [WebMethod]
        public Jogador login(string usuario, string senha)
        {
            CtrlJogador cJogador = new CtrlJogador();
            return cJogador.login(usuario, senha);
        }
    }
}