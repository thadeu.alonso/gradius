﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;

using Gradius.DAO.model;
using Gradius.DAO.controller;

namespace Gradius.ClienteServidor
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener = null;

            IPAddress endereco;
            int porta = 5000;

            Servidor servidor;
            bool rodando = true;

            try
            {
                endereco = IPAddress.Parse("127.0.0.1");
                servidor = new Servidor(endereco, porta);

                listener = new TcpListener(endereco, porta);
                listener.Start();

                while (rodando)
                {
                    if (!servidor.Pronto)
                    {
                        Console.WriteLine("Aguardando conexoes...");
                        TcpClient newClient = listener.AcceptTcpClient();

                        int newClientId = servidor.adicionaJogador(newClient);

                        PlayerConnectedMsg msg = new PlayerConnectedMsg(newClientId);
                        string json = JsonConvert.SerializeObject(msg);

                        servidor.SendTo(newClientId, json);
                        servidor.estaPronto();
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("Erro de rede: {0}", e);
            }
        }
    }
}