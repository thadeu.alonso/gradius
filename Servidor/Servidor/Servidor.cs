﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;

namespace Gradius.ClienteServidor
{
    class Servidor
    {
        private const int MAX_CLIENTS = 2;

        private int id = 0;
        private List<Jogador> jogadores;

        public bool Pronto { get; set; }

        public Servidor(IPAddress endereco, int porta)
        {
            jogadores = new List<Jogador>(MAX_CLIENTS);
        }

        public int adicionaJogador(TcpClient cliente)
        {
            if(id > MAX_CLIENTS - 1)
            {
                DesconnectClient(cliente);
                Pronto = true;
                return -1;
            }

            int newId = proximoId();
            Jogador jogador = new Jogador(newId, this, cliente);

            jogadores.Add(jogador);

            Console.WriteLine("Novo jogador conectado com o Id: " + newId);

            return newId;
        }

        public void setCodJogador(int id, int cod)
        {
            jogadores[id].CodJogador = cod;
        }

        private int proximoId()
        {
            return id++;
        }

        private void DesconnectClient(TcpClient cliente)
        {
            NetworkStream stream = cliente.GetStream();
            StreamWriter writer = new StreamWriter(stream);

            Console.WriteLine("Servidor lotado!");
            writer.WriteLine("Servidor lotado!");
            writer.Flush();
            writer.Close();

            cliente.Close();
        }

        public void estaPronto()
        {
            if (jogadores.Count == 2)
            {
                GameStartedMsg msg = new GameStartedMsg();
                string json = JsonConvert.SerializeObject(msg);

                SendToAll(json);

                PlayerLoggedMsg loggedMsg = new PlayerLoggedMsg(jogadores[0].CodJogador, jogadores[1].CodJogador);
                string loginJson = JsonConvert.SerializeObject(loggedMsg);
                SendToAll(loginJson);

                Console.WriteLine("Jogo iniciado!");
                Pronto = true;
            }
        }

        public void SendToAll(string dados)
        {
            foreach (Jogador jogador in jogadores)
            {
                jogador.envia(dados);
            }
        }

        public void SendTo(int id, string dados)
        {
            foreach (Jogador jogador in jogadores)
            {
                if (jogador.Id == id)
                {
                    jogador.envia(dados);
                    return;
                }
            }
        }

        public void SendToAllExcept(int id, string dados)
        {
            foreach (Jogador jogador in jogadores)
            {
                if(jogador.Id != id)
                    jogador.envia(dados);
            }
        }
    }
}