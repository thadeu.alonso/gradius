﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;

namespace Gradius.ClienteServidor
{
    class Jogador
    {
        public int Id { get; set; }
        public int CodJogador { get; set; }

        private Servidor        servidor;
        private TcpClient       cliente;

        private StreamReader    reader;
        private StreamWriter    writer;

        private Thread          thread;

        public Jogador(int _id, Servidor _servidor, TcpClient _cliente)
        {
            Id          = _id;
            servidor    = _servidor;
            cliente     = _cliente;

            NetworkStream stream = cliente.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);

            thread = new Thread(run);
            thread.Start();
        }

        public void run()
        {
            string dados = reader.ReadLine();

            while (dados != null)
            {
                Message msg = JsonConvert.DeserializeObject<Message>(dados);

                if (msg.MessageType == MessageType.PLAYER_LOGGED)
                {
                    PlayerLoggedMsg logged = JsonConvert.DeserializeObject<PlayerLoggedMsg>(dados);

                    int cod = Id == 0 ? logged.CodP1 : logged.CodP2;

                    servidor.setCodJogador(Id, cod);
                }
                else
                {
                    servidor.SendToAllExcept(Id, dados);
                }

                dados = reader.ReadLine();
            }

            cliente.Close();
        }

        public void envia(string dados)
        {
            Console.WriteLine("Enviando para o id: " + Id + " | Mensagem: " + dados);
            writer.WriteLine(dados);
            writer.Flush();
        }
    }
}