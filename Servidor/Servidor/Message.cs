﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gradius.ClienteServidor
{
    public enum MessageType
    {
        PLAYER_CONNECTED,
        PLAYER_MOVE,
        PLAYER_SHOOT,
        PLAYER_DIES,
        PLAYER_LOGGED,
        OBSTACLE_SPAWNED,
        OBSTACLE_DESTROYED,
        GAME_STARTED,
        GAME_OVER
    }

    [Serializable]
    public class Message
    {
        public MessageType MessageType { get; set; }
        public int PlayerId { get; set; }
    }

    [Serializable]
    public class GameStartedMsg : Message
    {
        public GameStartedMsg()
        {
            MessageType = MessageType.GAME_STARTED;
        }
    }

    [Serializable]
    public class GameOverMsg : Message
    {
        public GameOverMsg()
        {
            MessageType = MessageType.GAME_OVER;
        }
    }

    [Serializable]
    public class PlayerLoggedMsg : Message
    {
        public int CodP1 { get; set; }
        public int CodP2 { get; set; }

        public PlayerLoggedMsg(int _codP1, int _codP2)
        {
            MessageType = MessageType.PLAYER_LOGGED;
            CodP1 = _codP1;
            CodP2 = _codP2;
        }
    }

    [Serializable]
    public class PlayerConnectedMsg : Message
    {
        public PlayerConnectedMsg(int _newId)
        {
            MessageType = MessageType.PLAYER_CONNECTED;
            PlayerId = _newId;
        }
    }
}